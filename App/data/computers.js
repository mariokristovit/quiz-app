const questions = [
  {
    question: "Siapa nama raja Bajak laut pertama?",
    answers: [
      { id: "1", text: "Monkey D Luffy" },
      { id: "2", text: "Gold D Roger", correct: true },
      { id: "3", text: "Portgas D Ace" },
      { id: "4", text: "Monkey D Garp" }
    ]
  },
  {
    question: "Siapa nama kru pertama yang direkrut Luffy?",
    answers: [
      { id: "1", text: "Nami" },
      { id: "2", text: "Chopper" },
      { id: "3", text: "Sanji" },
      { id: "4", text: "Zoro", correct: true }
    ]
  }
];

export default questions;
