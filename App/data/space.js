const questions = [
  {
    question: "Apa nama planet urutan kelima dari matahari?",
    answers: [
      { id: "1", text: "Mars" },
      { id: "2", text: "Jupiter", correct: true },
      { id: "3", text: "Saturn" },
      { id: "4", text: "Venus" }
    ]
  },
  {
    question: "How many planets are in the Solar System?",
    answers: [
      { id: "1", text: "6" },
      { id: "2", text: "7" },
      { id: "3", text: "8", correct: true },
      { id: "4", text: "9" }
    ]
  }
];

export default questions;
