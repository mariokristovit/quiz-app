import React from "react";
import { View, ScrollView, Button, StatusBar } from "react-native";
import spaceQuestions from "../App/data/space";
import westernQuestions from "../App/data/westerns";
import computerQuestions from "../App/data/computers";

import { RowItem } from "../App/component/RowItem";

export default ({ navigation }) => (
  <ScrollView>
    <StatusBar barStyle="dark-content" />
    <RowItem 
        name="Space" 
        color="#36b1f0" 
        onPress={() => 
        navigation.navigate('Quiz',{
            title: "Space",
            questions: spaceQuestions,
            color: "#36b1f0"
    })} />
    <RowItem 
        name="Westerns" 
        color="#799496" 
        onPress={() => 
        navigation.navigate('Quiz',{
            title: "Western",
            questions: westernQuestions,
            color: "#799496"
        })
            
    } />
    <RowItem 
        name="Computers" 
        color="#49475B"
        onPress={() => 
        navigation.navigate('Quiz',{
            title: "Computers",
            questions: computerQuestions,
            color: "#49475B"})
     } />
  </ScrollView>
);
