import React, { Component } from "react";
import { View, StyleSheet, StatusBar, Text, SafeAreaView } from "react-native";
import questions from "../App/data/computers";
import { Button, ButtonContainer } from "../App/component/Button";
import { Alert } from '../App/component/Alert';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "skyblue",
    paddingHorizontal: 20
  },
  textCss: {
    color: "#fff",
    fontSize: 25,
    textAlign: "center",
    letterSpacing: -0.02,
    fontWeight: "600"
  },
  safe: {
    flex: 1,
    marginTop: 100,
    justifyContent: "space-between"
  }
});

class Quiz extends Component {
    state = {
        totalSoal: this.props.navigation.getParam("questions",[]).length,
        jumlahBenar: 0,
        activeQuestionIndex: 0,
        answered:false,
        answerCorrect:false

    }

    answer = (correct) =>{
        this.setState(state => {
            const nextState= {
              answered : true,
            };

            if (correct){
                nextState.jumlahBenar = state.jumlahBenar+1;
                nextState.answerCorrect = true;
            }else{
              nextState.answerCorrect = false;
            }

            return nextState;
        },
        () => {
            setTimeout(() => this.nextQuestion(), 750);
        } 
        )
    }

    nextQuestion = () => {
      this.setState(state => {
        let nextIndex =state.activeQuestionIndex +1;

        if (nextIndex >= state.totalSoal){
          nextIndex = 0;
        }

        return {
          activeQuestionIndex : nextIndex,
          answered: false
        }
      })
    }

  render() {
    const questions = this.props.navigation.getParam("questions",[])
    ask = questions[this.state.activeQuestionIndex];
    return (
      <View style={[
        styles.container, {backgroundColor:this.props.navigation.getParam("color")}]}>
        <StatusBar barStyle="Dark-content" />
        <SafeAreaView style={styles.safe}>
          <View>
            <Text style={styles.textCss}>{ask.question}</Text>

            <ButtonContainer>
              {ask.answers.map(answer => (
                <Button
                  key={answer.id}
                  text={answer.text}
                  onPress={() => this.answer(answer.correct)}
                />
              ))}
            </ButtonContainer>
          </View>

          <Text style={styles.textCss}>{this.state.jumlahBenar}/{this.state.totalSoal}</Text>
        </SafeAreaView>
        <Alert correct={this.state.answerCorrect} visible={this.state.answered}/>
      </View>
    );
  }
}

export default Quiz;
